package com.sixteeneyes.daligoapi.entity.KickBoard;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardUseRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistory {
    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //회원
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    //킥보드
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kickBoardId", nullable = false)
    private KickBoard kickBoard;

    //시작위도
    @Column(nullable = false)
    private Double startPosX;

    //시작경도
    @Column(nullable = false)
    private Double startPosY;

    //시작일
    @Column(nullable = false)
    private LocalDateTime dateStart;

    //종료위도
    private Double endPosX;

    //종료경도
    private Double endPosY;

    // 기준일
    private LocalDate dateBase;

    //종료일
    private LocalDateTime dateEnd;

    //이용금액
    @Column(nullable = false)
    private BigDecimal resultPrice;

    //완료여부
    @Column(nullable = false)
    private Boolean isComplete;

    public void putUseEnd(KickBoardUseRequest request) {
        this.endPosX = request.getPosX();
        this.endPosY = request.getPosY();
        this.dateBase = LocalDate.now();
        this.dateEnd = LocalDateTime.now();
    }

    public void putEndPrices(double resultPrice) {
        this.resultPrice = CommonFormat.convertDoubleToDecimal(resultPrice);
        this.isComplete = true;
    }

    private KickBoardHistory(Builder builder) {
        this.member = builder.member;
        this.kickBoard = builder.kickBoard;
        this.startPosX = builder.startPosX;
        this.startPosY = builder.startPosY;
        this.dateStart = builder.dateStart;
        this.resultPrice = builder.resultPrice;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistory> {
        private final Member member;
        private final KickBoard kickBoard;
        private final Double startPosX;
        private final Double startPosY;
        private final LocalDateTime dateStart;
        private final BigDecimal resultPrice;
        private final Boolean isComplete;

        public Builder(Member member, KickBoard kickBoard, KickBoardUseRequest request) {
            this.member = member;
            this.kickBoard = kickBoard;
            this.startPosX = request.getPosX();
            this.startPosY = request.getPosY();
            this.dateStart = LocalDateTime.now();
            this.resultPrice = CommonFormat.convertDoubleToDecimal(0D);
            this.isComplete = false;
        }

        @Override
        public KickBoardHistory build() {
            return new KickBoardHistory(this);
        }
    }
}
