package com.sixteeneyes.daligoapi.entity.KickBoard;

import com.sixteeneyes.daligoapi.enums.KickBoardStatus;
import com.sixteeneyes.daligoapi.enums.PriceBasis;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardRequest;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoard {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 킥보드 상태
    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private KickBoardStatus kickBoardStatus;

    // 모델명
    @Column(nullable = false, length = 20)
    private String modelName;

    // 고유번호
    @Column(nullable = false, length = 6, unique = true)
    private String uniqueNumber;

    // 기준요금
    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private PriceBasis priceBasis;

    // 구입일
    @Column(nullable = false)
    private LocalDate dateBuy;

    // 위도
    @Column(nullable = false)
    private Double posX;

    // 경도
    @Column(nullable = false)
    private Double posY;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

    // 사용여부
    @Column(nullable = false)
    private Boolean isUse;

    public void putKickBoard(KickBoardUpdateRequest request) {
        this.posX = request.getPosX();
        this.posY = request.getPosY();
        this.isUse = request.getIsUse();
    }


    public void putStatus(KickBoardStatus kickBoardStatus) {
        this.kickBoardStatus = kickBoardStatus;
    }

    public void putMemo(String memo) {
        this.memo = memo;
    }

    private KickBoard(Builder builder) {
        this.kickBoardStatus = builder.kickBoardStatus;
        this.modelName = builder.modelName;
        this.uniqueNumber = builder.uniqueNumber;
        this.priceBasis = builder.priceBasis;
        this.dateBuy = builder.dateBuy;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.isUse = builder.isUse;
    }

    public static class Builder implements CommonModelBuilder<KickBoard> {
        private final KickBoardStatus kickBoardStatus;
        private final String modelName;
        private final String uniqueNumber;
        private final PriceBasis priceBasis;
        private final LocalDate dateBuy;
        private final Double posX;
        private final Double posY;
        private final Boolean isUse;

        public Builder(KickBoardRequest request) {
            this.kickBoardStatus = KickBoardStatus.READY;
            this.modelName = request.getModelName();
            this.uniqueNumber = request.getUniqueNumber();
            this.priceBasis = request.getPriceBasis();
            this.dateBuy = request.getDateBuy();
            this.posX = request.getPosX();
            this.posY = request.getPosY();
            this.isUse = true;
        }

        @Override
        public KickBoard build() {
            return new KickBoard(this);
        }
    }
}
