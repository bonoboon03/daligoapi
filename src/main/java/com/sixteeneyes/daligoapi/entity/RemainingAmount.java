package com.sixteeneyes.daligoapi.entity;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RemainingAmount {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    // 잔여금액
    @Column(nullable = false)
    private BigDecimal remainPrice;

    public void putAmount(double price) {
        BigDecimal bigDecimalPrice = CommonFormat.convertDoubleToDecimal(price);
        this.remainPrice = this.remainPrice.add(bigDecimalPrice);
    }

    private RemainingAmount(Builder builder) {
        this.member = builder.member;
        this.remainPrice = builder.remainPrice;
    }

    public void putPriceMinus(BigDecimal price) {
        this.remainPrice.subtract(price);
    }

    public static class Builder implements CommonModelBuilder<RemainingAmount> {
        private final Member member;
        private final BigDecimal remainPrice;

        public Builder(Member member) {
            this.member = member;
            this.remainPrice = CommonFormat.convertDoubleToDecimal(0D);
        }

        @Override
        public RemainingAmount build() {
            return new RemainingAmount(this);
        }
    }
}
