package com.sixteeneyes.daligoapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
