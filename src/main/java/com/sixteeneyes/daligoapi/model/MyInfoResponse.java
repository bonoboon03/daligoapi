package com.sixteeneyes.daligoapi.model;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.entity.RemainingAmount;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyInfoResponse {
    @ApiModelProperty(notes = "이름", required = true)
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    private String phoneNumber;

    @ApiModelProperty(notes = "보유 금액", required = true)
    private BigDecimal price;

    @ApiModelProperty(notes = "생년월일", required = true)
    private String dateBirth;

    private MyInfoResponse(Builder builder) {
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.price = builder.price;
        this.dateBirth = builder.dateBirth;
    }

    public static class Builder implements CommonModelBuilder<MyInfoResponse> {
        private final String name;
        private final String phoneNumber;
        private final BigDecimal price;
        private final String dateBirth;

        public Builder(Member member, RemainingAmount remainingAmount) {
            this.name = member.getName();
            this.phoneNumber = member.getPhoneNumber();
            this.price = remainingAmount.getRemainPrice();
            this.dateBirth = CommonFormat.convertLocalDateToString(member.getDateBirth());
        }

        @Override
        public MyInfoResponse build() {
            return new MyInfoResponse(this);
        }
    }
}
