package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.enums.PriceBasis;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class KickBoardRequest {
    @ApiModelProperty(notes = "모델명(3~20)", required = true)
    @NotNull
    @Length(min = 3, max = 20)
    private String modelName;

    @ApiModelProperty(notes = "고유번호(6)", required = true)
    @NotNull
    @Length(min = 6, max = 6)
    private String uniqueNumber;

    @ApiModelProperty(notes = "기준요금", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PriceBasis priceBasis;

    @ApiModelProperty(notes = "구입일", required = true)
    @NotNull
    private LocalDate dateBuy;

    @ApiModelProperty(notes = "위도", required = true)
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "경도", required = true)
    @NotNull
    private Double posY;
}
