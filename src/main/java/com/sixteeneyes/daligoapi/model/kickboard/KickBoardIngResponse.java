package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardIngResponse {
    @ApiModelProperty(notes = "시퀀스", required = true)
    private Long historyId;

    @ApiModelProperty(notes = "고유번호", required = true)
    private String uniqueNumber;

    @ApiModelProperty(notes = "시작 위도", required = true)
    private Double startPosX;

    @ApiModelProperty(notes = "시작 경도", required = true)
    private Double startPosY;

    @ApiModelProperty(notes = "시작일", required = true)
    private String dateStart;

    @ApiModelProperty(notes = "기준요금", required = true)
    private String priceBasis;

    @ApiModelProperty(notes = "기본요금", required = true)
    private BigDecimal basePrice;

    @ApiModelProperty(notes = "분당 추가 요금", required = true)
    private BigDecimal perMinPrice;

    private KickBoardIngResponse(Builder builder) {
        this.historyId = builder.historyId;
        this.uniqueNumber = builder.uniqueNumber;
        this.startPosX = builder.startPosX;
        this.startPosY = builder.startPosY;
        this.dateStart = builder.dateStart;
        this.priceBasis = builder.priceBasis;
        this.basePrice = builder.basePrice;
        this.perMinPrice = builder.perMinPrice;
    }

    public static class Builder implements CommonModelBuilder<KickBoardIngResponse> {
        private final Long historyId;
        private final String uniqueNumber;
        private final Double startPosX;
        private final Double startPosY;
        private final String dateStart;
        private final String priceBasis;
        private final BigDecimal basePrice;
        private final BigDecimal perMinPrice;

        public Builder(Long historyId, String uniqueNumber, double startPosX, double startPosY, String dateStart, String priceBasis, double basePrice, double perMinPrice) {
            this.historyId = historyId;
            this.uniqueNumber = uniqueNumber;
            this.startPosX = startPosX;
            this.startPosY = startPosY;
            this.dateStart = dateStart;
            this.priceBasis = priceBasis;
            this.basePrice = CommonFormat.convertDoubleToDecimal(basePrice);
            this.perMinPrice = CommonFormat.convertDoubleToDecimal(perMinPrice);
        }

        @Override
        public KickBoardIngResponse build() {
            return new KickBoardIngResponse(this);
        }
    }
}
