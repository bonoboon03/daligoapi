package com.sixteeneyes.daligoapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class UseStaticsHistoryResponse {
    @ApiModelProperty(notes = "총 금액", required = true)
    private BigDecimal totalPrice;
}