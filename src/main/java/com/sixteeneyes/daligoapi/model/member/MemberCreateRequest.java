package com.sixteeneyes.daligoapi.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberCreateRequest {
    @ApiModelProperty(notes = "아이디(5~30)", required = true)
    @NotNull
    @Length(min = 5, max = 30)
    private String username;

    @ApiModelProperty(notes = "비밀 번호(8~20)", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String password;

    @ApiModelProperty(notes = "비밀 번호 확인(8~20)", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String passwordRe;

    @ApiModelProperty(notes = "이름(2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "연락처(13)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;

    @ApiModelProperty(notes = "면허증 번호(15)", required = true)
    @NotNull
    @Length(min = 15, max = 15)
    private String licenseNumber;

    @ApiModelProperty(notes = "생년월일", required = true)
    @NotNull
    private LocalDate dateBirth;
}
