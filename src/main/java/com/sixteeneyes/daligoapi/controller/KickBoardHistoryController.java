package com.sixteeneyes.daligoapi.controller;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoard;
import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.enums.KickBoardStatus;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardHistoryItem;
import com.sixteeneyes.daligoapi.model.UseStaticsHistoryResponse;
import com.sixteeneyes.daligoapi.model.common.CommonResult;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.model.common.SingleResult;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardHistoryResponse;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardIngResponse;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardUseRequest;
import com.sixteeneyes.daligoapi.service.RemainAmountService;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import com.sixteeneyes.daligoapi.service.kickBoard.KickBoardHistoryService;
import com.sixteeneyes.daligoapi.service.kickBoard.KickBoardService;
import com.sixteeneyes.daligoapi.service.member.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "킥보드 이용 내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kick-board-history")
public class KickBoardHistoryController {
    private final KickBoardHistoryService kickBoardHistoryService;
    private final ProfileService profileService;
    private final KickBoardService kickBoardService;
    private final RemainAmountService remainAmountService;

    @ApiOperation(value = "킥보드 이용 시작")
    @PostMapping("/kick-board-id/{kickBoardId}")
    public CommonResult setUseStart(@PathVariable long kickBoardId, @RequestBody @Valid KickBoardUseRequest request) {
        Member member = profileService.getMemberData();
        KickBoard kickBoard = kickBoardService.getData(kickBoardId);
        kickBoardHistoryService.setUseStart(member, kickBoard, request);

        // 킥보드 상태를 사용중으로 변경한다.
        kickBoardService.putStatus(kickBoard, KickBoardStatus.ING);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "킥보드 이용 종료")
    @PutMapping("/kick-board-id/{kickBoardId}")
    public CommonResult putUseEnd(@PathVariable long kickBoardId, @RequestBody @Valid KickBoardUseRequest request) {
        // 이용내역을 종료로 마감시킨다.
        KickBoardHistory kickBoardHistory = kickBoardHistoryService.setUseEnd(kickBoardId, request);

        // 확정된 이용요금을 회원 잔액에서 차감시킨다.
        remainAmountService.putAmountMinus(kickBoardHistory.getMember(), kickBoardHistory.getResultPrice());

        // 포인트 적립
        kickBoardHistory.getMember().putPoint(kickBoardHistory.getResultPrice());

        // 킥보드 상태를 대기로 변경한다.
        kickBoardService.putStatus(kickBoardHistory.getKickBoard(), KickBoardStatus.READY);

        return ResponseService.getSuccessResult();

    }

    @ApiOperation(value = "나의 킥보드 사용 내역")
    @GetMapping("/histories")
    public ListResult<KickBoardHistoryItem> getUseHistories(@RequestParam(value = "page", required = false, defaultValue = "1")int page) {
        Member member = profileService.getMemberData();
        return ResponseService.getListResult(kickBoardHistoryService.getUseHistories(member, page), true);
    }

    @ApiOperation(value = "나의 킥보드 사용 상세 내역")
    @GetMapping("/history")
    public SingleResult<KickBoardHistoryResponse> getUseHistory(@RequestParam(value = "id") long id) {
        return ResponseService.getSingleResult(kickBoardHistoryService.getUseHistory(id));
    }

    @ApiOperation(value = "사용 중인 킥보드")
    @GetMapping("/ing")
    public SingleResult<KickBoardIngResponse> getIngKickBoard() {
        Member member = profileService.getMemberData();
        return ResponseService.getSingleResult(kickBoardHistoryService.getIngKickBoard(member));
    }


    @ApiOperation("매출 통계")
    @GetMapping("/stats/total-price")
    public SingleResult<UseStaticsHistoryResponse> getTotalPrice(
            @RequestParam("dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam("dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd
    ) {
        return ResponseService.getSingleResult(kickBoardHistoryService.getStaticsHistoryPrice(dateStart, dateEnd));
    }
}
