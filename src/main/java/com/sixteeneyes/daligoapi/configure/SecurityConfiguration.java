package com.sixteeneyes.daligoapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;

    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(AUTH_WHITELIST);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManagerBean();
    }

    /*
    flutter 에서 header에 token 넣는법
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!; <-- 한줄 추가

    --- 아래 동일
     */

    // 퍼미션이 무엇인지 보기.
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic().disable()
                .cors().configurationSource(corsConfigurationSource())
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                        .antMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체 허용

                        // 회원
                        .antMatchers("/v1/member/join").permitAll() // 회원가입
                        .antMatchers("/v1/member/login/**").permitAll() // 로그인
                        .antMatchers("/v1/member/find-username").permitAll() // 아이디 찾기
                        .antMatchers("/v1/member/find-password").permitAll() // 비밀번호 찾기
                        .antMatchers("/v1/member/member-update").hasAnyRole("USER") // 회원 수정
                        .antMatchers("/v1/member/my-info").hasAnyRole("USER") // 나의 정보
                        .antMatchers("/v1/member/admin-update").hasAnyRole("ADMIN") // 관리자 수정
                        .antMatchers("/v1/member/password").hasAnyRole("ADMIN", "USER") // 비밀번호 변경

                        // 인증번호
                        .antMatchers("/v1/auth-phone/auth/start").permitAll() // 인증번호 등록
                        .antMatchers("/v1/auth-phone/auth/end").permitAll() // 인증번호 받기

                        // 리뷰
                        .antMatchers("/v1/review/all").permitAll() // 후기 리스트
                        .antMatchers("/v1/review/review").permitAll() // 후기 리스트 상세

                        // 금액 충전
                        .antMatchers("/v1/amount-charging/data").hasAnyRole("USER") // 금액 충전
                        .antMatchers("/v1/amount-charging/my-charging").hasAnyRole("USER") // 나의 금액 충전 리스트

                        // 잔여 금액
                        .antMatchers("/v1/remain-amount/remain-price").hasAnyRole("USER")

                        // 킥보드
                        .antMatchers("/v1/kick-board/near").hasAnyRole("USER") // 근처 킥보드 리스트

                        // 킥보드 사용
                        .antMatchers("/v1/kick-board-history/histories").hasAnyRole("USER") // 나의 킥보드 사용 내역
                        .antMatchers("/v1/kick-board-history/history").hasAnyRole("USER") // 나의 킥보드 사용 내역 리스트
                        .antMatchers("/v1/kick-board-history/kick-board-id/**").hasAnyRole("USER") // 킥보드 사용 시작, 종료
                        .antMatchers("/v1/kick-board-history/ing").hasAnyRole("USER") // 사용 중인 킥보드

                        .antMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN")
                        .antMatchers("/v1/auth-test/test-user").hasAnyRole("USER")
                        .antMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER")
                        .anyRequest().hasRole("ADMIN") // 기본 접근 권한은 ROLE_ADMIN
                .and()
                    .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                    .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .and()
                    .addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
       CorsConfiguration configuration = new CorsConfiguration();

       configuration.addAllowedOriginPattern("*");
       configuration.addAllowedHeader("*");
       configuration.addAllowedMethod("*");
       configuration.setAllowCredentials(true);

       UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
       source.registerCorsConfiguration("/**", configuration);
       return source;
    }
}
