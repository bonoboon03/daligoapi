package com.sixteeneyes.daligoapi.service.member;

import com.sixteeneyes.daligoapi.entity.AuthPhone;
import com.sixteeneyes.daligoapi.enums.MessageTemplate;
import com.sixteeneyes.daligoapi.exception.CAlreadyExistAuthException;
import com.sixteeneyes.daligoapi.exception.CMissingDataException;
import com.sixteeneyes.daligoapi.exception.CNotMatchAuthException;
import com.sixteeneyes.daligoapi.model.member.AuthPhoneCompleteRequest;
import com.sixteeneyes.daligoapi.model.member.AuthPhoneSendRequest;
import com.sixteeneyes.daligoapi.repository.AuthPhoneRepository;
import com.sixteeneyes.daligoapi.service.message.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class AuthPhoneService {
    private final AuthPhoneRepository authPhoneRepository;
    private final MessageService messageService; // service를 위한 service

    public void setAuthNumber(AuthPhoneSendRequest request) {
        Optional<AuthPhone> authPhoneCheckData = authPhoneRepository.findByPhoneNumber(request.getPhoneNumber());

        if (authPhoneCheckData.isPresent()) throw new CAlreadyExistAuthException(); // authPhoneCheckData가 있으면 던지기 또 인증받게 하면 안 되기 때문에

        String resultRandomNumber = makeRandomNumber(); // random값을 이 자리에 미리 저장해둠

        AuthPhone authPhone = new AuthPhone.Builder(request.getPhoneNumber(), resultRandomNumber).build();
        authPhoneRepository.save(authPhone); // 인증번호 생성해서 DB에 저장한 후 인증번호 발송

        HashMap<String, String> msgmap = new HashMap<>();
        msgmap.put("인증번호", resultRandomNumber);

        messageService.sendMessage(
                MessageTemplate.TD_8763, // 이 내용을 보내겠다
                "1577-5555", // 발신자 번호
                request.getPhoneNumber(), // 받는 사람 번호는 Request안에 있기 때문에 Request 안에 getPhoneNumber 해서 번호 가져오고
                msgmap
        );
    }

    public void putAuthStatus(AuthPhoneCompleteRequest request) {
        Optional<AuthPhone> authPhone = authPhoneRepository.findByPhoneNumber(request.getPhoneNumber());

        // 만약.... 데이터가 없다면... 중간에 해커가 데이터 변조한거임..그러니까 던져야함..
        if (authPhone.isEmpty()) throw new CMissingDataException();

        // 만약.. 인증번호가 일치하지 않는다면.... 상태를 바꾸지 못함... 던져야함..
        if (!authPhone.get().getAuthNumber().equals(request.getAuthNumber())) throw new CNotMatchAuthException();

        AuthPhone authPhoneOrigin = authPhone.get();
        authPhoneOrigin.putComplete();
        authPhoneRepository.save(authPhoneOrigin);
    }
    private String makeRandomNumber() {
        // 엄....예린시치님 로직 코드 참고해서 넣기..
        Random random = new Random(); // 랜덤 숫자 생성
        int createNum = 0; // 하나의 숫자를 만들어서 기본값 0으로 초기화 / createNum은 한 자리의 랜덤 숫자를 만듦
        String ranNum = ""; // 랜덤 숫자가 들어갈 자리 생성
        String resultNum = ""; // 결과 숫자가 들어갈 자리 생성

        for (int i=0; i<6; i++) { // int 타입으로 줘서 i=0는 초기값으로 설정하고 i<6 은 6번 돈다는 걸 의미하고 i++ 초기값으로 설정된 사이클 횟수를 늘려준다.
            // 얘가 6자리를 만들기 위해선 하나의 랜덤숫자를 6번 반복해야됨
            createNum = random.nextInt(9); // 0부터 9까지의 랜덤 숫자를 생성함 for문이니까 반복
            ranNum = Integer.toString(createNum); // 나온 int타입의 랜덤 숫자를 String 타입으로 줘서 문자로 바꿈
            // 6자리로 랜덤숫자를 만들어야 하는데 숫자끼리 더하면 6자리가 나오지 않고 문자로 바꿔야 6자리로 나열할 수 있기 때문에 int타입을 String 타입으로 변환한다. 1 + 2 = 3 / "1"+"2" = 12
            resultNum += ranNum; // 결과 값은 랜덤으로 나온 숫자 6개를 전부 더한 값이다.
        }
        return resultNum; // resultNum 를 return한다.
    }
}
