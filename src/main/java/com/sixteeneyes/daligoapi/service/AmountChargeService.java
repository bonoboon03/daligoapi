package com.sixteeneyes.daligoapi.service;

import com.sixteeneyes.daligoapi.entity.AmountChargingHistory;
import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.model.AmountChargingItem;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.repository.AmountChargingHistoryRepository;
import com.sixteeneyes.daligoapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AmountChargeService {
    private final AmountChargingHistoryRepository amountChargingHistoryRepository;

    public void setAmountCharging(Member member, double price) {
        AmountChargingHistory amountChargingHistory = new AmountChargingHistory.Builder(member, price).build();
        amountChargingHistoryRepository.save(amountChargingHistory);
    }

    public ListResult<AmountChargingItem> getList(int page){
        Page<AmountChargingHistory> originList = amountChargingHistoryRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<AmountChargingItem> result = new LinkedList<>();
        for (AmountChargingHistory amountChargingHistory : originList) {
            result.add(new AmountChargingItem.Builder(amountChargingHistory).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public ListResult<AmountChargingItem> getMyList(Member member, int page) {
        Page<AmountChargingHistory> originList = amountChargingHistoryRepository.findAllByMemberAndAndIdGreaterThanEqualOrderByIdDesc(member, 1, ListConvertService.getPageable(page));
        List<AmountChargingItem> result = new LinkedList<>();
        for (AmountChargingHistory amountChargingHistory : originList) {
            result.add(new AmountChargingItem.Builder(amountChargingHistory).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }
}