package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.entity.Member.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface KickBoardHistoryRepository extends JpaRepository<KickBoardHistory, Long> {
    Optional<KickBoardHistory> findByMemberAndIsComplete(Member member, boolean isComplete);
    Page<KickBoardHistory> findAllByIdGreaterThanEqualAndMemberAndIsCompleteOrderByIdDesc(long id, Member member, boolean isComplete, Pageable pageable);
    List<KickBoardHistory> findAllByDateBaseBetween(LocalDate dateStart, LocalDate dateEnd);
}
